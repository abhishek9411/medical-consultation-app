# medical_consultation_app

Medical Consultation App 

# Description:- 

A medical consultant app is a mobile application designed to facilitate healthcare consultations and support between medical professionals and patients. Such an app can provide various features and functionalities, offering convenience and accessibility for both healthcare providers and patients

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://docs.flutter.dev/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://docs.flutter.dev/cookbook)

For help getting started with Flutter development, view the
[online documentation](https://docs.flutter.dev/), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

# Features 
 ## Appointments:- 
    * - patients can schedule appointments with medical professionals, including doctors, specialists, and consultants.
    * - The app can display the availability of healthcare providers, allowing users to choose a suitable time slot.

 ## Consultant:- 
     * - Users can connect with healthcare professionals through virtual consultations, enabling remote medical advice and treatment.

## Technology Stack

 Flutter: The app is developed using the Flutter framework, which enables cross-platform development with a single codebase.
 Dart: Dart is the programming language used for Flutter development.
 Other libraries/tools: [json_annotation, get_it, mobx, flutter_mobx, intl]


# Output screenshot

![Screenshot](https://gitlab.com/abhishek9411/imagesrepo/-/raw/main/Simulator%20Screenshot%20-%20iPhone%2014%20-%202023-10-31%20at%2010.55.44.png?ref_type=heads)

![Screenshot](https://gitlab.com/abhishek9411/imagesrepo/-/raw/main/Simulator%20Screenshot%20-%20iPhone%2014%20-%202023-10-31%20at%2010.56.00.png?ref_type=heads)

![Screenshot](https://gitlab.com/abhishek9411/imagesrepo/-/raw/main/Simulator%20Screenshot%20-%20iPhone%2014%20-%202023-10-31%20at%2010.56.00.png?ref_type=heads)

![Screenshot](https://gitlab.com/abhishek9411/imagesrepo/-/raw/main/Simulator%20Screenshot%20-%20iPhone%2014%20-%202023-10-31%20at%2016.37.11.png?ref_type=heads)

![Screenshot](https://gitlab.com/abhishek9411/imagesrepo/-/raw/main/Simulator%20Screenshot%20-%20iPhone%2014%20-%202023-10-31%20at%2010.56.22.png?ref_type=heads)

![Screenshot](https://gitlab.com/abhishek9411/imagesrepo/-/raw/main/Simulator%20Screenshot%20-%20iPhone%2014%20-%202023-10-31%20at%2010.56.30.png?ref_type=heads)

![Screenshot](https://gitlab.com/abhishek9411/imagesrepo/-/raw/main/Simulator%20Screenshot%20-%20iPhone%2014%20-%202023-10-31%20at%2010.56.42.png?ref_type=heads)


# Video 

[![Watch the video](https://gitlab.com/abhishek9411/imagesrepo/-/raw/main/Simulator%20Screenshot%20-%20iPhone%2014%20-%202023-10-31%20at%2010.55.44.png?ref_type=heads)](https://gitlab.com/abhishek9411/imagesrepo/-/raw/main/medicalCunsultantAppRecordiing.mov?ref_type=heads)

