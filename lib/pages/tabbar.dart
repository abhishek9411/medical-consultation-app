import 'package:flutter/material.dart';
import 'package:medical_consultation_app/pages/chatlist_page.dart';
import 'package:medical_consultation_app/pages/medical_cunsultant_homepage.dart';
import 'package:medical_consultation_app/utils/constants.dart';
import 'package:medical_consultation_app/utils/spacing.dart';

class TabbarPage extends StatefulWidget {
  const TabbarPage({super.key, required this.title});
  final String title;
  @override
  State<TabbarPage> createState() => _TabbarPageState();
}

class _TabbarPageState extends State<TabbarPage> {
  int _currentIndex = 0;

  final List<Widget> _tabs = [
    const MedicalCunsulTantHomePage(title: ''),
    const Center(child: Text(StringConstant.favourite)),
    const Center(child: Text(StringConstant.addAppointments)),
    ChatListPage(),
    const Center(child: Text(StringConstant.settings)),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _tabs[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentIndex,
        elevation: 5,
        selectedItemColor: Colors.green,
        backgroundColor: Colors.grey,
        type: BottomNavigationBarType.shifting,
        onTap: (int index) {
          setState(() {
            _currentIndex = index;
          });
        },
        items: [
          const BottomNavigationBarItem(
            icon: Icon(
              Icons.home,
              color: Colors.blue,
              size: Spacing.s28,
            ),
            label: '',
          ),
          const BottomNavigationBarItem(
            icon: Icon(
              Icons.favorite,
              color: Colors.blue,
              size: Spacing.s28,
            ),
            label: '',
          ),
          const BottomNavigationBarItem(
            icon: Icon(
              Icons.add_circle,
              color: Colors.blue,
              size: Spacing.s48,
            ),
            label: '',
          ),
          const BottomNavigationBarItem(
            icon: Icon(
              Icons.message,
              color: Colors.blue,
              size: Spacing.s28,
            ),
            label: '',
          ),
          const BottomNavigationBarItem(
              icon: Icon(
                Icons.settings,
                color: Colors.blue,
                size: Spacing.s28,
              ),
              label: '')
        ],
      ),
    );
  }
}
