import 'package:flutter/material.dart';
import 'package:medical_consultation_app/model/medical_detail.dart';
import 'package:medical_consultation_app/utils/constants.dart';

import '../utils/fontsize.dart';
import '../utils/spacing.dart';
import '../utils/utils.dart';

class AppointmentTodaySeeAllWidget extends StatelessWidget {
  final List<AppointmentsToday>? appointmentsToday;
  const AppointmentTodaySeeAllWidget(
      {Key? key, required this.appointmentsToday})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: const Text(StringConstant.appointments),
        ),
        body: Container(
            color: Colors.transparent,
            child: ListView.builder(
              padding: const EdgeInsets.only(top: Spacing.s20),
              itemCount: appointmentsToday?.length,
              itemBuilder: (context, index) {
                return Card(
                  color: Colors.transparent,
                  elevation: 1,
                  margin: const EdgeInsets.all(Spacing.s8), // Card margin
                  child: Container(
                    padding: const EdgeInsets.all(Spacing.s16),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(Spacing.s12),
                      color: Colors.blueAccent,
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            ClipRRect(
                              borderRadius: BorderRadius.circular(Spacing.s32),
                              // Card margin
                              child: Image.asset(
                                'assets/images/${appointmentsToday?[index].image}',
                                height: Spacing.s64,
                                width: Spacing.s64,
                                fit: BoxFit.cover,
                              ),
                            ),
                            InkWell(
                              onTap: () {},
                              child: ClipRRect(
                                borderRadius:
                                    BorderRadius.circular(Spacing.s28),
                                child: Image.asset(
                                  ImageConstant.chat,
                                  height: Spacing.s60,
                                  width: Spacing.s60,
                                  fit: BoxFit.fill,
                                ),
                              ),
                            )
                          ],
                        ),
                        const SizedBox(height: Spacing.s12),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  appointmentsToday?[index].doctorName ?? '',
                                  style: const TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.w600,
                                      fontSize: FontSize.f16),
                                ),
                                Text(appointmentsToday?[index].specialist ?? '',
                                    style: const TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.w400,
                                        fontSize: FontSize.f16))
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                Text(
                                    DateTimeUtils.getDatefromTimeStemp(
                                        appointmentsToday?[index].time ?? 0,
                                        DateTimeUtils.hhMmA),
                                    style: const TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.w600,
                                        fontSize: FontSize.f16)),
                                Text(
                                    DateTimeUtils.getDatefromTimeStemp(
                                        appointmentsToday?[index].date ?? 0,
                                        DateTimeUtils.ddmmyyyy),
                                    style: const TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.w400,
                                        fontSize: FontSize.f16))
                              ],
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                );
              },
            )),
      ),
    );
  }
}
