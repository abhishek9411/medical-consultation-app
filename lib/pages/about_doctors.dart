import 'package:flutter/material.dart';
import 'package:medical_consultation_app/utils/fontsize.dart';
import 'package:medical_consultation_app/utils/spacing.dart';
import 'package:medical_consultation_app/widgets/schedule_date_widget.dart';
import 'package:medical_consultation_app/widgets/schedule_timeslot_widget.dart';

import '../model/medical_detail.dart';
import '../utils/constants.dart';
import '../widgets/readmore_widget.dart';

class AboutDoctorPage extends StatefulWidget {
  final TopDoctors? doctorDetail;
  const AboutDoctorPage({Key? key, required this.doctorDetail})
      : super(key: key);

  @override
  _AboutDoctorPageState createState() => _AboutDoctorPageState();
}

class _AboutDoctorPageState extends State<AboutDoctorPage> {
  bool isExpanded = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage(ImageConstant.background),
            fit: BoxFit.cover,
          ),
        ),
        child: Column(
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                InkWell(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Container(
                      margin: const EdgeInsets.only(
                          top: Spacing.s48, left: Spacing.s16),
                      height: Spacing.s40,
                      width: Spacing.s40,
                      decoration: BoxDecoration(
                          color: Colors.white30,
                          borderRadius: BorderRadius.circular(Spacing.s28)),
                      child: const Icon(Icons.arrow_back)),
                ),
                Container(
                  margin: const EdgeInsets.only(top: Spacing.s48),
                  child: const Text(
                    StringConstant.doctorDetail,
                    style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: FontSize.f18,
                        color: Colors.white),
                  ),
                ),
                const SizedBox(
                  width: Spacing.s48,
                )
              ],
            ),
            const SizedBox(
              height: Spacing.s48,
            ),
            Stack(
              alignment: Alignment.center,
              children: [
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height - 140,
                  color: Colors.transparent,
                ),
                Positioned(
                  top: 45,
                  left: 0,
                  bottom: 0,
                  right: 0,
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height - 200,
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(Spacing.s20),
                          topRight: Radius.circular(
                              Spacing.s20) // Adjust the radius as needed
                          ),
                      color: Colors.white,
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(top: Spacing.s40),
                      child: ListView(
                        children: [
                          Align(
                            alignment: Alignment.center,
                            child: Text(
                              widget.doctorDetail?.doctorName ?? '',
                              style: const TextStyle(
                                  fontWeight: FontWeight.w800,
                                  fontSize: FontSize.f20),
                            ),
                          ),
                          Align(
                            alignment: Alignment.center,
                            child: Text(
                              widget.doctorDetail?.specialist ?? '',
                              style: const TextStyle(
                                  color: Color.fromRGBO(206, 206, 206, 1.0),
                                  fontWeight: FontWeight.w600,
                                  fontSize: FontSize.f16),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(Spacing.s20),
                            height: Spacing.s80,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(Spacing.s12),
                              color: const Color.fromRGBO(244, 248, 254, 1.0),
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Container(
                                  padding: const EdgeInsets.all(Spacing
                                      .s12), // Add right margin as a separator

                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      const Text(
                                        StringConstant.patients,
                                        style: TextStyle(
                                            fontWeight: FontWeight.w700,
                                            fontSize: FontSize.f16,
                                            color: Colors.grey),
                                      ),
                                      const SizedBox(
                                        height: Spacing.s8,
                                      ),
                                      Text(
                                        '${widget.doctorDetail?.patients ?? 0}',
                                        style: const TextStyle(
                                            fontWeight: FontWeight.w800,
                                            fontSize: FontSize.f16,
                                            color: Colors.blue),
                                      )
                                    ],
                                  ),
                                ),
                                SizedBox(
                                  width: 2,
                                  height: 30,
                                  child: Container(
                                    color: Colors.grey.withOpacity(0.5),
                                  ),
                                ),
                                Container(
                                  padding: const EdgeInsets.all(Spacing.s12),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      const Text(
                                        StringConstant.experinces,
                                        style: TextStyle(
                                            fontWeight: FontWeight.w700,
                                            fontSize: FontSize.f16,
                                            color: Colors.grey),
                                      ),
                                      const SizedBox(
                                        height: Spacing.s8,
                                      ),
                                      Text(
                                        '${widget.doctorDetail?.experiences ?? 0} Yrs',
                                        style: const TextStyle(
                                            fontWeight: FontWeight.w800,
                                            fontSize: FontSize.f16,
                                            color: Colors.blue),
                                      )
                                    ],
                                  ),
                                ),
                                SizedBox(
                                  width: 2,
                                  height: 30,
                                  child: Container(
                                    color: Colors.grey.withOpacity(0.5),
                                  ),
                                ),
                                Container(
                                  padding: const EdgeInsets.all(Spacing.s12),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      const Text(
                                        StringConstant.ratings,
                                        style: TextStyle(
                                            fontWeight: FontWeight.w700,
                                            fontSize: FontSize.f16,
                                            color: Colors.grey),
                                      ),
                                      const SizedBox(
                                        height: Spacing.s8,
                                      ),
                                      Text(
                                        '${widget.doctorDetail?.rating ?? 0}',
                                        style: const TextStyle(
                                            fontWeight: FontWeight.w800,
                                            fontSize: FontSize.f16,
                                            color: Colors.blue),
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                          Container(
                            padding: const EdgeInsets.only(
                                left: Spacing.s20, right: Spacing.s20),
                            child: Column(
                              children: [
                                const Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    StringConstant.aboutDoctor,
                                    style: TextStyle(
                                        fontWeight: FontWeight.w700,
                                        fontSize: FontSize.f18),
                                  ),
                                ),
                                const SizedBox(height: Spacing.s8),
                                ReadMoreText(
                                  longText: widget.doctorDetail?.about ?? '',
                                  maxLines: (Spacing.s80).toInt(),
                                )
                              ],
                            ),
                          ),
                          ScheduleDateWidget(doctorDetail: widget.doctorDetail),
                          const Divider(),
                          ScheduleTimeSlotWidget(
                              doctorDetail: widget.doctorDetail)
                        ],
                      ),
                    ),
                  ),
                ),
                Positioned(
                  top: 0,
                  child: Container(
                    width: 90,
                    height: 90,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(45),
                        color: Colors.green,
                        image: const DecorationImage(
                          image: AssetImage(ImageConstant
                              .doctor1), // Replace with your image path
                          fit:
                              BoxFit.cover, // Adjust the fit property as needed
                        )),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
