import 'package:flutter/material.dart';
import 'package:medical_consultation_app/utils/constants.dart';
import 'package:medical_consultation_app/utils/fontsize.dart';
import 'package:medical_consultation_app/utils/spacing.dart';
import 'package:medical_consultation_app/widgets/chatlist_widget.dart';

class ChatListPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          color: const Color.fromRGBO(247, 248, 250, 1.0),
          padding: const EdgeInsets.only(left: Spacing.s20, right: Spacing.s20),
          child: ListView(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text(
                    StringConstant.chat,
                    style: TextStyle(
                        fontWeight: FontWeight.bold, fontSize: FontSize.f24),
                  ),
                  const Icon(Icons.more_horiz)
                ],
              ),
              const SizedBox(
                height: Spacing.s20,
              ),
              Row(
                // mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Container(
                    height: Spacing.s52,
                    width: MediaQuery.of(context).size.width - 110,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(Spacing.s28),
                        color: Colors.white),
                    child: Row(
                      // mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const SizedBox(width: Spacing.s12),
                        Image.asset(
                          ImageConstant.search,
                          height: Spacing.s24,
                          width: Spacing.s24,
                          color: Colors.grey,
                        ),
                        const Expanded(
                          child: TextField(
                            style: TextStyle(
                                color: Colors.grey, fontSize: FontSize.f14),
                            decoration: InputDecoration(
                              hintText: StringConstant.searchHere,
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide.none,
                              ),
                              focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide.none),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(
                    width: Spacing.s20,
                  ),
                  Flexible(
                    child: Container(
                      height: Spacing.s48,
                      width: Spacing.s48,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(Spacing.s12),
                          color: Colors.white),
                      child: const Icon(Icons.edit_note),
                    ),
                  ),
                ],
              ),
              const ChatListWidget(),
            ],
          )),
    );
  }
}
