import 'package:flutter/material.dart';
import 'package:medical_consultation_app/utils/constants.dart';

import '../model/medical_detail.dart';
import '../utils/fontsize.dart';
import '../utils/spacing.dart';
import 'about_doctors.dart';

class TopDoctorsPages extends StatelessWidget {
  final List<TopDoctors>? topDoctors;

  const TopDoctorsPages({Key? key, required this.topDoctors}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text(StringConstant.doctors),
        ),
        body: ListView.builder(
          shrinkWrap: true,
          itemCount: topDoctors?.length,
          itemBuilder: (context, index) {
            return InkWell(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return AboutDoctorPage(
                        doctorDetail: topDoctors?[index],
                      ); // NewScreen is the widget for the screen you want to navigate to.
                    },
                  ),
                );
              },
              child: Card(
                elevation: 1, // Card elevation (shadow)
                margin: const EdgeInsets.all(Spacing.s8), // Card margin
                child: Row(
                  // mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                        padding: const EdgeInsets.all(Spacing.s12),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(5.0),
                          child: Image.asset(
                              'assets/images/${topDoctors?[index].image}', // Replace with the image URL or asset path
                              width: 70, // Adjust the width as needed
                              height: 80, // Adjust the height as needed
                              fit: BoxFit
                                  .cover // Adjust the fit property as needed

                              ),
                        )),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          topDoctors?[index].specialist ?? '',
                          style: const TextStyle(
                              color: Colors.grey, fontSize: FontSize.f14),
                        ),
                        Text(
                          topDoctors?[index].doctorName ?? '',
                          style: const TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: FontSize.f16),
                        ),
                        Row(
                          children: [
                            const Icon(
                              Icons.star,
                              color: Colors.yellow,
                              size: FontSize.f14,
                            ),
                            Text(
                              '${topDoctors?[index].rating ?? ''} * ${topDoctors?[index].review ?? ''} ${StringConstant.reviews}',
                              style: const TextStyle(fontSize: FontSize.f14),
                            ),
                          ],
                        )
                      ],
                    ),
                  ],
                ),
              ),
            );
          },
        ));
  }
}
