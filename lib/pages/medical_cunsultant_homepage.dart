import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:medical_consultation_app/pages/topdoctors_pages.dart';
import 'package:medical_consultation_app/utils/constants.dart';

import '../main.dart';
import '../store/medical_detailstore.dart';
import '../utils/spacing.dart';
import '../widgets/hospital_fasility_widget.dart';
import '../widgets/searchbar_widget.dart';
import '../widgets/seeall_header_widget.dart';
import '../widgets/top_doctors_widget.dart';
import 'about_doctors.dart';
import 'appointment_today_see_all_page.dart';

class MedicalCunsulTantHomePage extends StatefulWidget {
  const MedicalCunsulTantHomePage({super.key, required this.title});
  final String title;
  @override
  State<MedicalCunsulTantHomePage> createState() =>
      _MedicalCunsulTantHomePageState();
}

class _MedicalCunsulTantHomePageState extends State<MedicalCunsulTantHomePage> {
  final medicalStore = getIt.get<MedicalDetailStore>();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(child: Scaffold(
      body: SingleChildScrollView(
        // padding: const EdgeInsets.only(left: 0, right: 20, top: 0),
        child: Observer(
          builder: (context) {
            return Container(
              padding: const EdgeInsets.only(
                  left: Spacing.s20, right: Spacing.s20, top: Spacing.s0),
              color: const Color.fromRGBO(247, 248, 250, 1.0),
              child: Column(
                children: [
                  Container(
                    height: Spacing.s80,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'Hello, ${medicalStore.medicalDetailModel.value?.name ?? ""}',
                              style: const TextStyle(
                                  fontSize: 22, fontWeight: FontWeight.bold),
                            ),
                            Text(medicalStore.medicalDetailModel.value?.title ??
                                ""),
                          ],
                        ),
                        Image.asset(
                            'assets/images/${medicalStore.medicalDetailModel.value?.image}',
                            width: Spacing.s50,
                            height: Spacing.s50,
                            fit: BoxFit.cover)
                      ],
                    ),
                  ),
                  SearchBarWidget(onChange: (text) {}),
                  HospitalFasilityWidget(
                      hospitalFasilityType: medicalStore
                          .medicalDetailModel.value?.hospitalFasilityType,
                      onTap: (index) {}),
                  SellAllHeaderWidget(
                    appointmentsToday: medicalStore
                        .medicalDetailModel.value?.appointmentsToday,
                    title: StringConstant.appointmentToday,
                    onTap: (index) {},
                    onTapSeeAll: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) {
                            return AppointmentTodaySeeAllWidget(
                                appointmentsToday: medicalStore
                                    .medicalDetailModel
                                    .value
                                    ?.appointmentsToday);
                          },
                        ),
                      );
                    },
                  ),
                  TopDoctorsWidget(
                    topDoctors:
                        medicalStore.medicalDetailModel.value?.topDoctors ?? [],
                    onTap: (index) {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) {
                            return AboutDoctorPage(
                              doctorDetail: medicalStore
                                  .medicalDetailModel.value?.topDoctors?[index],
                            );
                          },
                        ),
                      );
                    },
                    onTapSeeAll: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) {
                            return TopDoctorsPages(
                                topDoctors: medicalStore
                                    .medicalDetailModel.value?.topDoctors);
                          },
                        ),
                      );
                    },
                  )
                ],
              ),
            );
          },
        ),
      ),
    ));
  }
}
