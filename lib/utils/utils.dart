import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:medical_consultation_app/utils/constants.dart';

class DateTimeUtils {
  static const hhMmA = 'hh:mm a';
  static const ddmmyyyy = 'dd, MMM yyyy';
  static const mm = 'MM';
  static const ee = 'EE';
  static const dd = 'dd';

  static String getDatefromTimeStemp(int timeStemp, String dateFormat) {
    DateTime dateTime = DateTime.fromMillisecondsSinceEpoch(timeStemp);
    String time = DateFormat(dateFormat).format(dateTime);
    return time;
  }
}

class AlertBox {
  static void showAlertDialog(BuildContext context, String msg) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text(StringConstant.appName),
          content: Text(msg),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: const Text(StringConstant.ok),
            ),
          ],
        );
      },
    );
  }
}
