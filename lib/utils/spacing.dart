class Spacing {
  static const s0 = 0.0;
  static const s1 = 1.0;
  static const s2 = 2.0;
  static const s4 = 4.0;
  static const s8 = 8.0;
  static const s12 = 12.0;
  static const s16 = 16.0;
  static const s20 = 20.0;
  static const s24 = 24.0;
  static const s28 = 28.0;
  static const s32 = 32.0;
  static const s36 = 36.0;
  static const s40 = 40.0;
  static const s48 = 48.0;
  static const s50 = 50.0;
  static const s52 = 52.0;
  static const s56 = 56.0;
  static const s60 = 60.0;
  static const s64 = 64.0;
  static const s80 = 80.0;
  static const s120 = 120.0;
}
