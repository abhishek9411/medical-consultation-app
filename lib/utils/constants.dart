class StringConstant {
  static const scheduleAppointmentText = 'Your appointment has been scheduled';
  static const scheduleAppointmentTitle = 'Schedule Appointment';
  static const morning = 'Morning';
  static const selectSchedule = 'Select Schedule';
  static const aboutDoctor = 'About Doctor';
  static const ratings = 'Ratings';
  static const reviews = 'Reviews';
  static const appName = 'Medical Appointment App';
  static const experinces = 'Experinces';
  static const patients = 'Patients';
  static const doctorDetail = 'Detail Doctor';
  static const appointments = 'Appointments';
  static const chat = 'Chat';
  static const searchHere = 'Search here...';
  static const appointmentToday = 'Appointment Today';
  static const favourite = 'favourite';
  static const addAppointments = 'Add Appointments';
  static const settings = 'Settings';
  static const doctors = 'Doctors';
  static const ok = 'OK';
  static const readLess = 'Read less';
  static const readMore = 'Read more';
  static const seeAll = 'See All';
  static const topDoctors = 'Top doctor\'s for you';
}

class ImageConstant {
  static const doctor1 = 'assets/images/doctor1.jpeg';
  static const background = 'assets/images/background.jpeg';
  static const chat = 'assets/images/chat.jpeg';
  static const search = 'assets/images/search.png';
  static const avatar = 'assets/images/avatar.jpeg';
}

class JsonConstant {
  static const chatJson = 'assets/json/chat.json';
  static const medicalDetailJson = 'assets/json/medical_DetailJson.json';
}
