class FontSize {
  static const f2 = 0.0;
  static const f4 = 4.0;
  static const f8 = 8.0;
  static const f10 = 10.0;

  static const f12 = 12.0;
  static const f14 = 14.0;
  static const f16 = 16.0;
  static const f18 = 18.0;
  static const f20 = 20.0;
  static const f24 = 24.0;
  static const f28 = 28.0;
  static const f32 = 32.0;
}
