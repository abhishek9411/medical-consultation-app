import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:medical_consultation_app/utils/spacing.dart';

import '../main.dart';
import '../store/chat_list_store.dart';
import '../utils/fontsize.dart';
import '../utils/utils.dart';

class ChatListWidget extends StatelessWidget {
  const ChatListWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Observer(builder: (context) {
      final chatStore = getIt.get<ChatListStore>();
      return ListView.builder(
        shrinkWrap: true,
        padding: const EdgeInsets.only(top: Spacing.s8),
        itemCount: chatStore.chatMessages.value?.chats?.length,
        itemBuilder: (context, index) {
          return InkWell(
            child: Card(
                elevation: 1,
                margin: const EdgeInsets.all(Spacing.s8), // Card margin
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                        padding: const EdgeInsets.all(Spacing.s8),
                        child: Stack(
                          children: [
                            Positioned(
                                child: ClipRRect(
                              borderRadius: BorderRadius.circular(Spacing.s24),
                              child: Image.asset(
                                  'assets/images/${chatStore.chatMessages.value?.chats?[index].senderImage}',
                                  width: Spacing.s52,
                                  height: Spacing.s52,
                                  fit: BoxFit.cover),
                            )),
                            Positioned(
                                bottom: Spacing.s2,
                                right: Spacing.s2,
                                child: Container(
                                  height: Spacing.s12,
                                  width: Spacing.s12,
                                  decoration: BoxDecoration(
                                      color: chatStore.chatMessages.value
                                                  ?.chats?[index].isActive ??
                                              false
                                          ? Colors.green
                                          : Colors.grey,
                                      borderRadius: BorderRadius.circular(6)),
                                ))
                          ],
                        )),
                    Container(
                      width: MediaQuery.of(context).size.width - 140,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                chatStore.chatMessages.value?.chats?[index]
                                        .sender ??
                                    '',
                                style: const TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.w600,
                                    fontSize: FontSize.f16),
                              ),
                              Text(
                                DateTimeUtils.getDatefromTimeStemp(
                                    chatStore.chatMessages.value?.chats?[index]
                                            .date ??
                                        0,
                                    DateTimeUtils.hhMmA),
                                style: const TextStyle(
                                    color: Colors.grey,
                                    fontWeight: FontWeight.w600,
                                    fontSize: FontSize.f12),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                chatStore.chatMessages.value?.chats?[index]
                                        .text ??
                                    '',
                                style: const TextStyle(
                                    fontWeight: FontWeight.w400,
                                    fontSize: FontSize.f14,
                                    color: Colors.grey),
                              ),
                              Container(
                                height: Spacing.s16,
                                width: Spacing.s16,
                                decoration: BoxDecoration(
                                    color: Colors.blue,
                                    borderRadius:
                                        BorderRadius.circular(Spacing.s8)),
                                child: Center(
                                  child: Text(
                                    '${chatStore.chatMessages.value?.chats?[index].count ?? 0}',
                                    textAlign: TextAlign.center,
                                    style: const TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.w600,
                                        fontSize: FontSize.f10),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    )
                  ],
                )),
          );
        },
      );
    });
  }
}
