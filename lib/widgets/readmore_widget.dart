import 'package:flutter/material.dart';
import 'package:medical_consultation_app/utils/constants.dart';
import 'package:medical_consultation_app/utils/fontsize.dart';

class ReadMoreText extends StatefulWidget {
  final String longText;
  final int maxLines;
  final String readMoreText;
  final String readLessText;

  const ReadMoreText({
    required this.longText,
    this.maxLines = 12,
    this.readMoreText = StringConstant.readMore,
    this.readLessText = StringConstant.readLess,
  });

  @override
  _ReadMoreTextState createState() => _ReadMoreTextState();
}

class _ReadMoreTextState extends State<ReadMoreText> {
  bool isExpanded = true;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          widget.longText,
          maxLines: isExpanded ? 2 : widget.maxLines,
          overflow: TextOverflow.ellipsis,
          style: const TextStyle(
              color: Color.fromRGBO(197, 197, 197, 1.0),
              fontWeight: FontWeight.w500,
              fontSize: FontSize.f14),
        ),
        GestureDetector(
          onTap: () {
            setState(() {
              isExpanded = !isExpanded;
            });
          },
          child: Text(
            isExpanded ? widget.readMoreText : widget.readLessText,
            style: const TextStyle(
                color: Colors.blue,
                fontWeight: FontWeight.w500,
                fontSize: FontSize.f14),
          ),
        ),
      ],
    );
  }
}
