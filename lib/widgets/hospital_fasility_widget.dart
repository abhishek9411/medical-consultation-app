import 'package:flutter/material.dart';
import 'package:medical_consultation_app/model/medical_detail.dart';
import 'package:medical_consultation_app/utils/spacing.dart';

class HospitalFasilityWidget extends StatelessWidget {
  final List<HospitalFasilityType>? hospitalFasilityType;
  final Function(int) onTap;
  const HospitalFasilityWidget(
      {Key? key, required this.hospitalFasilityType, required this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(top: Spacing.s20),
      height: 110,
      child: GridView.builder(
        physics: const NeverScrollableScrollPhysics(),
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 4, // Number of columns
        ),
        itemBuilder: (context, index) {
          return Column(
            children: [
              InkWell(
                onTap: () {
                  onTap(index);
                },
                child: Container(
                    height: Spacing.s60,
                    width: Spacing.s60,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(30)),
                    child: Image.asset(
                      'assets/images/${hospitalFasilityType?[index].image ?? ''}',
                      width: Spacing.s20,
                      height: Spacing.s20,
                      // fit: BoxFit.cover,
                    )),
              ),
              Text(hospitalFasilityType?[index].title ?? '')
            ],
          );
        },
        itemCount: 4, // Total number of items in the grid
      ),
    );
  }
}
