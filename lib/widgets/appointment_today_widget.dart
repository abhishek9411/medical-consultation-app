import 'package:flutter/material.dart';
import 'package:medical_consultation_app/model/medical_detail.dart';
import 'package:medical_consultation_app/utils/constants.dart';
import 'package:medical_consultation_app/utils/fontsize.dart';
import 'package:medical_consultation_app/utils/spacing.dart';

import '../utils/utils.dart';

class AppointmentTodayWidget extends StatelessWidget {
  final List<AppointmentsToday>? appointmentsToday;
  final Function(int) onTap;
  final Function() onTapSeeAll;

  const AppointmentTodayWidget(
      {Key? key,
      required this.appointmentsToday,
      required this.onTap,
      required this.onTapSeeAll})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 180,
        color: Colors.transparent,
        child: GridView.builder(
          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 1, childAspectRatio: 0.55),
          itemCount: appointmentsToday?.length,
          scrollDirection: Axis.horizontal,
          itemBuilder: (context, index) {
            return Card(
              color: Colors.transparent,
              elevation: 1,
              margin: const EdgeInsets.all(Spacing.s8),
              child: Container(
                padding: const EdgeInsets.all(Spacing.s16),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(Spacing.s12),
                  color: Colors.blueAccent,
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        ClipRRect(
                          borderRadius: BorderRadius.circular(Spacing.s32),
                          // Card margin
                          child: Image.asset(
                            'assets/images/${appointmentsToday?[index].image}',
                            height: Spacing.s64,
                            width: Spacing.s64,
                            fit: BoxFit.cover,
                          ),
                        ),
                        InkWell(
                          onTap: () {},
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(30),
                            child: Image.asset(
                              ImageConstant.chat,
                              height: Spacing.s60,
                              width: Spacing.s60,
                              fit: BoxFit.fill,
                            ),
                          ),
                        )
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              appointmentsToday?[index].doctorName ?? '',
                              style: const TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w600,
                                  fontSize: FontSize.f16),
                            ),
                            Text(appointmentsToday?[index].specialist ?? '',
                                style: const TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.w400,
                                    fontSize: FontSize.f16))
                          ],
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Text(
                                DateTimeUtils.getDatefromTimeStemp(
                                    appointmentsToday?[index].time ?? 0,
                                    DateTimeUtils.hhMmA),
                                style: const TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.w600,
                                    fontSize: FontSize.f16)),
                            Text(
                                DateTimeUtils.getDatefromTimeStemp(
                                    appointmentsToday?[index].date ?? 0,
                                    DateTimeUtils.ddmmyyyy),
                                style: const TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.w400,
                                    fontSize: FontSize.f16))
                          ],
                        )
                      ],
                    )
                  ],
                ),
              ),
            );
          },
        ));
  }
}
