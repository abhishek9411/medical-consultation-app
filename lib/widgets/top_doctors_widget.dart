import 'package:flutter/material.dart';
import 'package:medical_consultation_app/utils/constants.dart';

import '../model/medical_detail.dart';
import '../utils/fontsize.dart';
import '../utils/spacing.dart';

class TopDoctorsWidget extends StatelessWidget {
  final List<TopDoctors>? topDoctors;
  final Function(int) onTap;
  final Function() onTapSeeAll;

  const TopDoctorsWidget(
      {Key? key,
      required this.topDoctors,
      required this.onTap,
      required this.onTapSeeAll})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            const Text(
              StringConstant.topDoctors,
              style: TextStyle(
                  fontWeight: FontWeight.w500, fontSize: FontSize.f16),
            ),
            TextButton(
                onPressed: onTapSeeAll,
                child: const Text(
                  StringConstant.seeAll,
                  style: TextStyle(color: Colors.blue, fontSize: FontSize.f16),
                ))
          ],
        ),
        ListView.builder(
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          itemCount: topDoctors?.length,
          itemBuilder: (context, index) {
            return InkWell(
              onTap: () {
                onTap(index);
              },
              child: Card(
                elevation: 1, // Card elevation (shadow)
                margin: const EdgeInsets.all(Spacing.s8), // Card margin
                child: Row(
                  // mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                        padding: const EdgeInsets.all(10),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(5.0),
                          child: Image.asset(
                              'assets/images/${topDoctors?[index].image}', // Replace with the image URL or asset path
                              width: 70, // Adjust the width as needed
                              height:
                                  Spacing.s80, // Adjust the height as needed
                              fit: BoxFit
                                  .cover // Adjust the fit property as needed

                              ),
                        )),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          topDoctors?[index].specialist ?? '',
                          style: const TextStyle(
                              color: Colors.grey, fontSize: FontSize.f16),
                        ),
                        Text(
                          topDoctors?[index].doctorName ?? '',
                          style: const TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: FontSize.f16),
                        ),
                        Row(
                          children: [
                            const Icon(
                              Icons.star,
                              color: Colors.yellow,
                              size: FontSize.f14,
                            ),
                            Text(
                              '${topDoctors?[index].rating ?? ''} * ${topDoctors?[index].review ?? ''} ${StringConstant.reviews}',
                              style: const TextStyle(fontSize: FontSize.f14),
                            ),
                          ],
                        )
                      ],
                    ),
                  ],
                ),
              ),
            );
          },
        )
      ],
    );
  }
}
