import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:medical_consultation_app/main.dart';

import '../model/medical_detail.dart';
import '../store/medical_detailstore.dart';
import '../utils/constants.dart';
import '../utils/fontsize.dart';
import '../utils/spacing.dart';
import '../utils/utils.dart';

class ScheduleDateWidget extends StatelessWidget {
  final TopDoctors? doctorDetail;

  const ScheduleDateWidget({Key? key, required this.doctorDetail})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Observer(builder: (context) {
      final medicalStore = getIt.get<MedicalDetailStore>();
      final selectedScheduleDate = medicalStore.selectedScheduleDate?.value;
      return Container(
        padding: const EdgeInsets.only(
            left: Spacing.s20, right: Spacing.s20, top: Spacing.s20),
        child: Column(
          children: [
            const Align(
              alignment: Alignment.centerLeft,
              child: Text(
                StringConstant.selectSchedule,
                style: TextStyle(
                    fontWeight: FontWeight.w700, fontSize: FontSize.f18),
              ),
            ),
            const SizedBox(height: 5),
            Container(
              height: Spacing.s120,
              child: Center(
                child: Container(
                  height: 90,
                  child: GridView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: doctorDetail?.availableDates?.length ?? 0,
                      gridDelegate:
                          const SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 1,
                              crossAxisSpacing: 0,
                              mainAxisSpacing: 0,
                              childAspectRatio: 1.4),
                      itemBuilder: (context, index) {
                        return InkWell(
                          onTap: () {
                            medicalStore.setSelectedScheduleDate(index);
                          },
                          child: Container(
                            padding: const EdgeInsets.all(5),
                            child: Container(
                              decoration: BoxDecoration(
                                  boxShadow: [
                                    BoxShadow(
                                      color: selectedScheduleDate == index
                                          ? Colors.blue
                                          : const Color.fromRGBO(244, 248, 254,
                                              1.0), // Shadow color
                                      blurRadius: 4, // Spread of the shadow
                                      offset: const Offset(-2,
                                          2), // Offset in the x and y direction
                                    ),
                                  ],
                                  color: selectedScheduleDate == index
                                      ? Colors.blue
                                      : const Color.fromRGBO(
                                          243, 247, 253, 1.0),
                                  borderRadius: BorderRadius.circular(10)),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                      DateTimeUtils.getDatefromTimeStemp(
                                          doctorDetail
                                                  ?.availableDates?[index] ??
                                              0,
                                          DateTimeUtils.ee),
                                      style: TextStyle(
                                          color: selectedScheduleDate == index
                                              ? Colors.white
                                              : Colors.grey,
                                          fontWeight: FontWeight.w600,
                                          fontSize: FontSize.f16)),
                                  const SizedBox(height: Spacing.s8),
                                  Text(
                                      DateTimeUtils.getDatefromTimeStemp(
                                          doctorDetail
                                                  ?.availableDates?[index] ??
                                              0,
                                          DateTimeUtils.dd),
                                      style: TextStyle(
                                          color: selectedScheduleDate == index
                                              ? Colors.white
                                              : Colors.black,
                                          fontWeight: FontWeight.w600,
                                          fontSize: FontSize.f16))
                                ],
                              ),
                            ),
                          ),
                        );
                      }),
                ),
              ),
            )
          ],
        ),
      );
    });
  }
}
