import 'package:flutter/material.dart';
import 'package:medical_consultation_app/utils/constants.dart';
import 'package:medical_consultation_app/utils/fontsize.dart';
import 'package:medical_consultation_app/utils/spacing.dart';

class SearchBarWidget extends StatelessWidget {
  final Function(String) onChange;
  const SearchBarWidget({Key? key, required this.onChange}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: Spacing.s48,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(Spacing.s24),
          color: Colors.white),
      child: Row(
        // mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const SizedBox(width: 10),
          Image.asset(
            ImageConstant.search,
            height: Spacing.s24,
            width: Spacing.s24,
            color: Colors.grey,
          ),
          Expanded(
            child: TextField(
              onChanged: onChange,
              style:
                  const TextStyle(color: Colors.grey, fontSize: FontSize.f14),
              decoration: const InputDecoration(
                hintText: StringConstant.searchHere,
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide.none,
                ),
                focusedBorder: OutlineInputBorder(borderSide: BorderSide.none),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
