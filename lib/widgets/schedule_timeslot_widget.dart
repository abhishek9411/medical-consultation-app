import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:medical_consultation_app/main.dart';
import 'package:medical_consultation_app/store/medical_detailstore.dart';

import '../model/medical_detail.dart';
import '../utils/constants.dart';
import '../utils/fontsize.dart';
import '../utils/spacing.dart';
import '../utils/utils.dart';

class ScheduleTimeSlotWidget extends StatelessWidget {
  final TopDoctors? doctorDetail;

  const ScheduleTimeSlotWidget({Key? key, required this.doctorDetail})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Observer(builder: (context) {
      final medicalStore = getIt.get<MedicalDetailStore>();
      final selectedScheduleTime = medicalStore.selectedScheduleTime.value;
      return Container(
        padding: const EdgeInsets.only(
            left: Spacing.s20, right: Spacing.s20, top: Spacing.s20),
        child: Column(
          children: [
            const Align(
              alignment: Alignment.centerLeft,
              child: Text(
                StringConstant.morning,
                style: TextStyle(
                    fontWeight: FontWeight.w700, fontSize: FontSize.f18),
              ),
            ),
            const SizedBox(
              height: Spacing.s4,
            ),
            Container(
              height: Spacing.s80,
              child: Center(
                child: Container(
                  height: Spacing.s60,
                  child: GridView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: doctorDetail?.availableDates?.length ?? 0,
                      gridDelegate:
                          const SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 1,
                              crossAxisSpacing: 0,
                              mainAxisSpacing: 0,
                              childAspectRatio: 0.5),
                      itemBuilder: (context, index) {
                        return InkWell(
                          onTap: () {
                            medicalStore.setSelectedScheduleTime(index);
                          },
                          child: Container(
                            padding: const EdgeInsets.all(5),
                            child: Container(
                              decoration: BoxDecoration(
                                  boxShadow: [
                                    BoxShadow(
                                      color: selectedScheduleTime == index
                                          ? Colors.blue
                                          : const Color.fromRGBO(243, 247, 253,
                                              1.0), // Shadow color
                                      blurRadius: 4, // Spread of the shadow
                                      offset: const Offset(-2,
                                          2), // Offset in the x and y direction
                                    ),
                                  ],
                                  color: selectedScheduleTime == index
                                      ? Colors.blue
                                      : const Color.fromRGBO(
                                          243, 247, 253, 1.0),
                                  borderRadius: BorderRadius.circular(10)),
                              child: Center(
                                child: Text(
                                    DateTimeUtils.getDatefromTimeStemp(
                                        doctorDetail?.timeSlots?[index] ?? 0,
                                        DateTimeUtils.hhMmA),
                                    style: TextStyle(
                                        color: selectedScheduleTime == index
                                            ? Colors.white
                                            : Colors.black,
                                        fontWeight: FontWeight.w600,
                                        fontSize: FontSize.f16)),
                              ),
                            ),
                          ),
                        );
                      }),
                ),
              ),
            ),
            Container(
              height: Spacing.s50,
              margin: const EdgeInsets.all(Spacing.s40),
              decoration: BoxDecoration(boxShadow: [
                const BoxShadow(
                  color: Colors.blue,
                  blurRadius: Spacing.s4,
                  offset: Offset(-2, 2),
                ),
              ], color: Colors.blue, borderRadius: BorderRadius.circular(10)),
              child: InkWell(
                onTap: () {
                  AlertBox.showAlertDialog(
                      context, StringConstant.scheduleAppointmentText);
                },
                child: const Align(
                  alignment: Alignment.center,
                  child: Text(
                    StringConstant.scheduleAppointmentTitle,
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w600,
                        fontSize: FontSize.f16),
                  ),
                ),
              ),
            )
          ],
        ),
      );
    });
  }
}
