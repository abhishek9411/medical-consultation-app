import 'package:flutter/material.dart';
import 'package:medical_consultation_app/utils/constants.dart';
import 'package:medical_consultation_app/widgets/appointment_today_widget.dart';

import '../model/medical_detail.dart';
import '../utils/fontsize.dart';

class SellAllHeaderWidget extends StatelessWidget {
  final List<AppointmentsToday>? appointmentsToday;
  final Function(int) onTap;
  final Function() onTapSeeAll;
  final String title;
  const SellAllHeaderWidget(
      {Key? key,
      required this.appointmentsToday,
      required this.title,
      required this.onTap,
      required this.onTapSeeAll})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: Container(
        height: 250,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  title,
                  style: const TextStyle(
                      fontWeight: FontWeight.w500, fontSize: FontSize.f16),
                ),
                TextButton(
                    onPressed: onTapSeeAll,
                    child: const Text(
                      StringConstant.seeAll,
                      style:
                          TextStyle(color: Colors.blue, fontSize: FontSize.f16),
                    ))
              ],
            ),
            AppointmentTodayWidget(
                appointmentsToday: appointmentsToday,
                onTap: (index) {},
                onTapSeeAll: () {})
          ],
        ),
      ),
    );
  }
}
