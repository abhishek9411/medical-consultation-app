import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:medical_consultation_app/pages/tabbar.dart';
import 'package:medical_consultation_app/store/chat_list_store.dart';
import 'package:medical_consultation_app/store/medical_detailstore.dart';
import 'package:medical_consultation_app/utils/constants.dart';

import 'main.dart';

class MedicalCulsultantApp extends StatefulWidget {
  const MedicalCulsultantApp({super.key});

  @override
  State<MedicalCulsultantApp> createState() => _MedicalCulsultantAppState();
}

class _MedicalCulsultantAppState extends State<MedicalCulsultantApp>
    with WidgetsBindingObserver {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    getIt.registerSingleton(MedicalDetailStore());
    getIt.registerSingleton(ChatListStore());
  }

  @override
  void dispose() {
    super.dispose();
    WidgetsBinding.instance.removeObserver(this);
  }

  @override
  Widget build(BuildContext context) => Observer(
        builder: (context) => const MaterialApp(
          color: Color(0x00eb4c43),
          debugShowCheckedModeBanner: false,
          home: TabbarPage(title: StringConstant.appName),
        ),
      );
}
