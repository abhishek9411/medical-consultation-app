import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:medical_consultation_app/medical_culsultant_app.dart';

GetIt getIt = GetIt.instance;

void main() {
  runApp(const MedicalCulsultantApp());
}
