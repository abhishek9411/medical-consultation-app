import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:medical_consultation_app/model/chat_messages.dart';
import 'package:mobx/mobx.dart';

import '../utils/constants.dart';

class ChatListStore {
  final Observable<ChatMessages?> chatMessages = Observable(ChatMessages());

  ChatListStore() {
    getChatMessages();
  }

  void getChatMessages() async {
    final String data = await rootBundle.loadString(JsonConstant.chatJson);
    final Map<String, dynamic> parsedData = json.decode(data);
    final model = ChatMessages.fromJson(parsedData);

    runInAction(() {
      chatMessages.value = model;
    });
  }
}
