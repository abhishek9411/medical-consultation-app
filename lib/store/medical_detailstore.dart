import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:medical_consultation_app/model/medical_detail.dart';
import 'package:mobx/mobx.dart';

import '../utils/constants.dart';

class MedicalDetailStore {
  final Observable<MedicalDetailModel?> medicalDetailModel =
      Observable(MedicalDetailModel());
  final Observable<int?> selectedScheduleDate = Observable(0);
  final Observable<int?> selectedScheduleTime = Observable(0);

  MedicalDetailStore() {
    getMedicalDetail();
  }

  void getMedicalDetail() async {
    final String data =
        await rootBundle.loadString(JsonConstant.medicalDetailJson);
    final Map<String, dynamic> parsedData = json.decode(data);
    final model = MedicalDetailModel.fromJson(parsedData);
    runInAction(() {
      medicalDetailModel.value = model;
    });
  }

  void setSelectedScheduleDate(int index) {
    runInAction(() {
      selectedScheduleDate.value = index;
    });
  }

  void setSelectedScheduleTime(int index) {
    runInAction(() {
      selectedScheduleTime.value = index;
    });
  }
}
