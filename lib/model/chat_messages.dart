import 'package:json_annotation/json_annotation.dart';

part 'chat_messages.g.dart';

@JsonSerializable()
class ChatMessages {
  List<Chats>? chats;

  ChatMessages({this.chats});
  factory ChatMessages.fromJson(Map<String, dynamic> json) =>
      _$ChatMessagesFromJson(json);
  Map<String, dynamic> toJson() => _$ChatMessagesToJson(this);
}

@JsonSerializable()
class Chats {
  String? text;
  String? sender;
  int? date;
  int? count;
  bool? isActive;
  String? senderImage;

  Chats(
      {this.text,
      this.sender,
      this.date,
      this.count,
      this.isActive,
      this.senderImage});

  factory Chats.fromJson(Map<String, dynamic> json) => _$ChatsFromJson(json);
  Map<String, dynamic> toJson() => _$ChatsToJson(this);
}
