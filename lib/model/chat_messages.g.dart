// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'chat_messages.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ChatMessages _$ChatMessagesFromJson(Map<String, dynamic> json) => ChatMessages(
      chats: (json['chats'] as List<dynamic>?)
          ?.map((e) => Chats.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$ChatMessagesToJson(ChatMessages instance) =>
    <String, dynamic>{
      'chats': instance.chats,
    };

Chats _$ChatsFromJson(Map<String, dynamic> json) => Chats(
      text: json['text'] as String?,
      sender: json['sender'] as String?,
      date: json['date'] as int?,
      count: json['count'] as int?,
      isActive: json['isActive'] as bool?,
      senderImage: json['senderImage'] as String?,
    );

Map<String, dynamic> _$ChatsToJson(Chats instance) => <String, dynamic>{
      'text': instance.text,
      'sender': instance.sender,
      'date': instance.date,
      'count': instance.count,
      'isActive': instance.isActive,
      'senderImage': instance.senderImage,
    };
