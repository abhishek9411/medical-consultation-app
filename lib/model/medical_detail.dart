import 'package:json_annotation/json_annotation.dart';

part 'medical_detail.g.dart';

@JsonSerializable()
class MedicalDetailModel {
  String? name;
  String? title;
  String? image;
  List<HospitalFasilityType>? hospitalFasilityType;
  List<AppointmentsToday>? appointmentsToday;
  List<TopDoctors>? topDoctors;

  MedicalDetailModel(
      {this.name,
      this.title,
      this.image,
      this.hospitalFasilityType,
      this.appointmentsToday,
      this.topDoctors});
  factory MedicalDetailModel.fromJson(Map<String, dynamic> json) =>
      _$MedicalDetailModelFromJson(json);
  Map<String, dynamic> toJson() => _$MedicalDetailModelToJson(this);
}

@JsonSerializable()
class HospitalFasilityType {
  String? title;
  String? image;

  HospitalFasilityType({this.title, this.image});
  factory HospitalFasilityType.fromJson(Map<String, dynamic> json) =>
      _$HospitalFasilityTypeFromJson(json);
  Map<String, dynamic> toJson() => _$HospitalFasilityTypeToJson(this);
}

@JsonSerializable()
class AppointmentsToday {
  String? doctorName;
  String? specialist;
  String? image;
  int? time;
  int? date;

  AppointmentsToday(
      {this.doctorName, this.specialist, this.image, this.time, this.date});

  factory AppointmentsToday.fromJson(Map<String, dynamic> json) =>
      _$AppointmentsTodayFromJson(json);
  Map<String, dynamic> toJson() => _$AppointmentsTodayToJson(this);
}

@JsonSerializable()
class TopDoctors {
  String? doctorName;
  String? specialist;
  String? rating;
  String? review;
  String? about;
  List<int>? availableDates;
  List<int>? timeSlots;
  int? patients;
  int? experiences;
  String? image;

  TopDoctors(
      {this.doctorName,
      this.specialist,
      this.rating,
      this.review,
      this.about,
      this.availableDates,
      this.timeSlots,
      this.patients,
      this.experiences,
      this.image});

  factory TopDoctors.fromJson(Map<String, dynamic> json) =>
      _$TopDoctorsFromJson(json);
  Map<String, dynamic> toJson() => _$TopDoctorsToJson(this);
}
